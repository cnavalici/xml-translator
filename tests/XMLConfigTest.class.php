<?php

// phpunit XMLConfigTest.class.php

require_once "../src/XMLConfig.class.php";
require_once "../src/XMLException.class.php";

class XMLConfigTest extends PHPUnit_Framework_TestCase {
    public function testEmptyLangFolder() {
        $this->setExpectedException('XT\XMLException');
        XT\XMLConfig::init();
        XT\XMLConfig::set_lang_folder('empty_lang');
    }
  
    public function testLangFolderOneFileZeroSize() {
        $this->setExpectedException('XT\XMLException');
        XT\XMLConfig::init();
        XT\XMLConfig::set_lang_folder('lang_one_file_zero_size');
    }
  
    
    public function testDefaultLangFolder() {
        $this->assertEquals('lang', XT\XMLConfig::get_default_lang_folder());
    }
    
    public function testGetLangFolderAfterInit() {
        XT\XMLConfig::refresh_instance();
        $this->assertEquals('lang', XT\XMLConfig::get_lang_folder());
    }

    public function testSetGetNewLangFolder() {
        XT\XMLConfig::refresh_instance();
        XT\XMLConfig::set_lang_folder('lang2');
        $this->assertEquals('lang2', XT\XMLConfig::get_lang_folder());
    }
    
    public function testBuildPathFolderEmptyLangCode() {
        XT\XMLConfig::refresh_instance();
        $expected = XT\XMLConfig::get_lang_folder().
                    DIRECTORY_SEPARATOR.
                    XT\XMLConfig::get_current_lang().
                    '.xml';
        $this->assertEquals($expected, XT\XMLConfig::build_path());
    }
    

    public function testBuildPathFolder() {
        XT\XMLConfig::refresh_instance();
        $name_lang = 'test';
        $expected = XT\XMLConfig::get_lang_folder().
                    DIRECTORY_SEPARATOR.
                    $name_lang.
                    '.xml';
        $this->assertEquals($expected, XT\XMLConfig::build_path($name_lang));
    }

    
    public function testGetAvailableLanguages() {
        $expected = array('en');
        $this->assertEquals($expected, XT\XMLConfig::get_available_languages());
    }
    
    
    public function testCheckLangInvalidValue() {
        $this->setExpectedException('XT\XMLException');
        XT\XMLConfig::check_lang('not_existent');
    }

    
    public function testCheckLangValidLang() {
        $this->assertTrue(XT\XMLConfig::check_lang('en'));
    }
    
    
    /*    
    public function setUp() {

    }
    
    public function tearDown() {

    }
    */
}