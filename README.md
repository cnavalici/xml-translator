XML-Translator
==============


What's this?
---------------
XML Translator - an xml based module for internationalization of the strings.


Prepare the installation
---------------

Unpack/copy the package at a convenient location. Make sure you have the _lang/_ folder
and at least one xml file in it. Check the provided files for a quick example.

The xml structure should look like:


    <?xml version="1.0" encoding="UTF-8"?>
        <languages>
            <language name="en">
                <string key="one">translated value</string>
                ...
            </language>
        </languages>


How to use it?
---------------

### Getting some initial information

#### Getting default/current languages folder

The current and default one is the same if you didn't change them already.
    
    $xmlconfig::get_default_lang_folder();
    $xmlconfig::get_lang_folder();
        

#### Available languages

They match the xml files from the languages folder.

    $availlang = $xmlconfig::get_available_languages();
    echo .implode(", ", $availlang);


### Getting translations

The methods are static so they can be called directly, without object instantiation.

For convenience only, I'll be using variables to keep the Singleton instances:

    $xmltrans = XT\XMLTrans::init();
    $xmlconfig = XT\XMLConfig::init();

#### Setting up the current language

    $xmlconfig::set_current_lang('en');
    
#### Getting translations for different keys

    $xmltrans::get_text('one');
    $xmltrans::get_text('note')


Note that all the operations are against the current selected language. If you need translation from
another language, you have to switch the language (see _setting up the current language_ above).


### Administration


#### Creating a new language file

This will create a new bg.xml file. Then, add more keys to it or check the syncronization chapter.

    $xmltrans::create_lang_file('bg');


#### Adding a new key

The first step is option but ensures that you have selected the proper language to work on.

    $xmlconfig::set_current_lang('en');
    $xmltrans::add_key('newkey', 'This is a new key');
    
    
#### Updating a key

Again, make sure you have selected the correct language.

    $xmlconfig::set_current_lang('en');
    $xmltrans::update_key('newkey', 'This is the new value for key');


#### Deleting a key

    $xmlconfig::set_current_lang('en');
    $xmltrans::delete_key('newkey');


#### Syncronize the language files


One of the coolest feature is this syncronization against a base file. Just make sure you have selected
your base language (in this case English):

    $xmlconfig::set_current_lang('en');
    
And then, update the rest of the files: add to them the missing keys (from base file) and delete the
extra ones (not found in the en.xml). After this operation, all language files should contain the same
amount of keys (of course, you need to update the translation for the newly generated keys).

    $xmltrans::syncronize_lang_files();