<?php
/**
 * XML TRANSLATOR EXAMPLES
 *
 * how to use it in your scripts (read this file before run it on your webserver)
 *
 * make sure you have the folder lang, with at least one language file inside.
 * see the provided ones for examples
 */

// include this file
require_once 'src/XMLTrans.class.php';

// call this just one time; they are Singletons instances
$xmltrans = XT\XMLTrans::init();
$xmlconfig = XT\XMLConfig::init();

// how to extract some information about configuration
echo "Default Languages Folder: " .$xmlconfig::get_default_lang_folder()."\n";
echo "Current Languages Folder: ". $xmlconfig::get_lang_folder()."\n";
$availlang = $xmlconfig::get_available_languages();
echo "Available languages: ".implode(", ", $availlang)."\n";

// how to use a language
echo "Setting current language to english\n";
$xmlconfig::set_current_lang('en');
echo "Translate key 'one': ".$xmltrans::get_text('one')."\n";
echo "Translate key 'note': ".$xmltrans::get_text('note')."\n";

echo "Setting current language to romanian\n";
$xmlconfig::set_current_lang('ro');
echo "Translate key 'one': ".$xmltrans::get_text('one')."\n";
echo "Translate key 'note': ".$xmltrans::get_text('note')."\n";

echo "And again to russian\n";
$xmlconfig::set_current_lang('ru');
echo "Translate key 'one': ".$xmltrans::get_text('one')."\n";
echo "Translate key 'note': ".$xmltrans::get_text('note')."\n";

// admin mode - adding a new key
echo "Adding a new key english language file\n";
$xmlconfig::set_current_lang('en');
$xmltrans::add_key('newkey', 'This is a new key');
echo "Translate key 'newkey': ".$xmltrans::get_text('newkey')."\n";

$xmlconfig::set_current_lang('en');
$xmltrans::syncronize_lang_files();


