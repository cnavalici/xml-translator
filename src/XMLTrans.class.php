<?php
/**
 * XML TRANSLATOR MODULE
 *
 * an i18n module using xml as the internal engine
 *
 * @package XMLTranslator
 */

namespace XT;
 
require_once "XMLConfig.class.php";
require_once "XMLException.class.php";

class XMLTrans {
    private static $instance;
    private static $cache = array();
    
    protected static $config;
    
    private function __construct() {
        mb_internal_encoding("UTF-8");
        XMLConfig::init();
    }
    
    private function __clone() {}

    
    public static function init() {
        if (!XMLTrans::$instance instanceof self) {
            XMLTrans::$instance = new self();
        }

        return XMLTrans::$instance;
    }
    
    
    public static function get_text($key) {
        $e = self::extract_text($key, XMLConfig::get_current_lang());

        if (!$e) $e = self::extract_text($key, XMLConfig::get_default_lang());
        
        return $e;
    }
    

    public static function add_key($key, $val = '') {
        if (self::get_text($key)) return false;
        
        $current_file = XMLConfig::build_path();
        $dom = self::create_dom_connector($current_file);
        
        $newelem = $dom->createElement("string", $val);
        $newelem->setAttribute("key", $key);
        
        $node = $dom->getElementsByTagName("language")->item(0);        
        $node->appendChild($newelem);

        $dom->save($current_file);        
    }
    
    
    public static function update_key($key, $newval = '') {
        $current_file = XMLConfig::build_path();
        $dom = self::create_dom_connector($current_file);

        $newnode = $dom->createElement("string", $newval);
        $newnode->setAttribute("key", $key);
        
        $oldnode = self::extract_node($key, $dom);
        
        $oldnode->parentNode->replaceChild($newnode, $oldnode);
        $dom->save($current_file);
    }
    

    public static function delete_key($key) {
        $current_file = XMLConfig::build_path();
        $dom = self::create_dom_connector($current_file);

        $oldnode = self::extract_node($key, $dom);
        
        $oldnode->parentNode->removeChild($oldnode);
        $dom->save($current_file);        
    }


    public static function create_lang_file($lang_code) {
        if (in_array($lang_code, XMLConfig::get_available_languages())) {
            throw new XMLException(__METHOD__." The language file for $lang_code already exists.");
        }
        
        $new_file = XMLConfig::build_path($lang_code);
        $dom = self::create_dom_connector();

        $root_element = $dom->createElement('languages');
        $lang_element = $dom->createElement('language');
        $lang_element->setAttribute('name', $lang_code);

        $root_element->appendChild($lang_element);
        $dom->appendChild($root_element);

        $dom->save($new_file);
        
        XMLConfig::refresh_instance();
    }

    
    public static function syncronize_lang_files() {
        $current_keys = self::extract_keys(XMLConfig::build_path());
        
        $avail = XMLConfig::get_available_languages();
        $current = array(XMLConfig::get_current_lang());
        $other_langs = array_diff($avail, $current);

        $current_lang = XMLConfig::get_current_lang();
        foreach ($other_langs as $lang) {
            $other_keys = self::extract_keys(XMLConfig::build_path($lang));
            
            $extra_keys = array_diff($current_keys, $other_keys);
            self::append_extra_keys($extra_keys, $lang);            
            
            $not_needed_keys = array_diff($other_keys, $current_keys);
            self::remove_extra_keys($not_needed_keys, $lang);            
        }
        XMLConfig::set_current_lang($current_lang);
    }


    private static function append_extra_keys($extra_keys, $lang) {
        if (!$extra_keys) return;

        XMLConfig::set_current_lang($lang);        
        foreach($extra_keys as $key) {
            self::add_key($key, '');
        }
    }


    private static function remove_extra_keys($extra_keys, $lang) {
        if (!$extra_keys) return;

        XMLConfig::set_current_lang($lang);        
        foreach($extra_keys as $key) {
            self::delete_key($key);
        }
    }
    
    
    private static function extract_keys($lang_file) {
        $dom = self::create_dom_connector($lang_file);
        foreach($dom->getElementsByTagName('string') as $aa) {
            $keys[] = $aa->getAttribute('key');
        }
        return $keys;
    }
    
    
    private static function extract_text($key, $lang_code) {
        $xml = self::read_lang_file($lang_code);
        $xpath = $xml->xPath("//string[@key='$key']");
        return $xpath ? (string)$xpath[0] : '';
    }
    

    private static function extract_node($key, $dom) {
        $xpath = new \DOMXpath($dom);
        $oldnode = $xpath->query("//string[@key='$key']")->item(0);
        return ($oldnode) ? $oldnode : false;
    }
    
    
    private static function create_dom_connector($file = '') {
        $dom = new \DOMDocument('1.0', 'utf-8');
        // this should stay before load() to format output otherwise it doesn't
        $dom->preserveWhiteSpace = false;
        if ($file) $dom->load($file);
        $dom->formatOutput = TRUE;

        return $dom;        
    }

    
    private static function read_lang_file($lang_code = '') {
        XMLConfig::check_lang($lang_code);
        $filename = XMLConfig::build_path($lang_code);
        $md5file = md5_file($filename);
        
        if (array_key_exists($md5file, self::$cache)) {
            return self::$cache[$md5file];
        }

        $xml = simplexml_load_file($filename);
        self::$cache[md5_file($filename)] = $xml;
        
        return $xml;
    }
}
