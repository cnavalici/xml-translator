<?php
/**
 * XML CONFIG MODULE
 *
 * @package XMLTranslator
 */

namespace XT;

require_once "XMLException.class.php";

class XMLConfig {
    protected static $lang_folder = '';
    protected static $available_languages = array();
    
    private static $default_lang_folder = 'lang';
    private static $default_lang = '';
    private static $current_lang = '';
    private static $instance;

    private function __clone() {}

    
    public static function init() {
        if (!XMLConfig::$instance instanceof self) {
            self::$lang_folder = self::$default_lang_folder;
            self::check_for_available_languages();
            self::set_default_lang(self::$available_languages[0]);
            self::set_current_lang(self::$available_languages[0]);
            
            XMLConfig::$instance = new self();
        }

        return XMLConfig::$instance;
    }

    
    public static function get_lang_folder() {
        return self::$lang_folder;
    }

    
    public static function get_default_lang_folder() {
        return self::$default_lang_folder;
    }

    
    public static function set_lang_folder($lang_folder = '') {
        if (!is_dir($lang_folder)) throw new XMLException("Not a valid language folder ($lang_folder).");
        
        self::$lang_folder = rtrim($lang_folder, '/');
        self::check_for_available_languages();
    }
    
    
    public static function get_available_languages() {
        return self::$available_languages;
    }


    public static function set_default_lang($lang = '') {
        self::check_lang($lang);
        self::$default_lang = $lang;
    }


    public static function get_default_lang() { return self::$default_lang; }
    
    
    public static function set_current_lang($lang = '') {
        self::check_lang($lang);
        self::$current_lang = $lang;        
    }

    
    public static function get_current_lang() { return self::$current_lang; }
    

    public static function build_path($lang_code = '') {
        if (!$lang_code) $lang_code = self::$current_lang;

        return  rtrim(self::$lang_folder, '/\\').
                DIRECTORY_SEPARATOR.
                $lang_code.
                ".xml";
    }
    

    public static function check_lang($lang = '') {
        if (!in_array($lang, self::$available_languages)) {
            throw new XMLException(__METHOD__." Specified $lang is not available");
        }
        return true;
    }    


    public static function refresh_instance() {
        self::$instance = null;
        self::init();
    }
    
    
    private static function check_for_available_languages() {
        self::$available_languages = array();
        
        $langfiles = new \DirectoryIterator(self::$lang_folder);
        
        foreach($langfiles as $lang) {
            if (!$lang->getSize()) {
                throw new XMLException(__METHOD__." - One or more language files have 0 size");
            }
            
            if ($lang->getExtension() == 'xml') {
                self::$available_languages[] = $lang->getBasename('.xml');
            }
        }
        
        if (!self::$available_languages) {
            throw new XMLException(__METHOD__." - No languages files found.");
        }
    }

}