<?php
/**
 * XML Exception
 *
 * @package XMLTranslator
 */

namespace XT;
 
class XMLException extends \Exception {
    public function __toString() {
        $msg = str_replace("XT\\", "", $this->message);
        return "$msg\n";
    }    
}
